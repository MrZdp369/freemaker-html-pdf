package cn.com.sinosoft.app.pdf.headfoot;

import java.awt.Color;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

/**
 * Tools: font settings
 */
public class Fonts_self {
	private static BaseFont default_font;
	private static BaseFont default_font2;
	private static BaseFont default_font3;
	public static Color white = new Color(0xFF, 0xFF, 0xFF);
	public  static Color TableHeaderBlue = new Color(0, 111, 192);
	public  static Color backgroudWhite = white;
	public static Color blue = new Color(0, 111, 192);
	public static Color black = new Color(0, 0, 0);
	public Fonts_self(String t_binDir) throws DocumentException, IOException{
//		URL fontOne=Fonts_self.class.getResource("/fonts/MSYH.TTF");
//		URL fontTwo=Fonts_self.class.getResource("/fonts/ERASDEMI.TTF");
//		URL fontThree=Fonts_self.class.getResource("/fonts/IMPRISHA.TTF");
		
		default_font  =  BaseFont.createFont("/fonts/MSYH.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		default_font2=BaseFont.createFont("/fonts/ERASDEMI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		default_font3=BaseFont.createFont("/fonts/IMPRISHA.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		
//		default_font  =  BaseFont.createFont(t_binDir+"resources/fonts/MSYH.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//		default_font2=BaseFont.createFont(t_binDir+"resources/fonts/ERASDEMI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//		default_font3=BaseFont.createFont(t_binDir+"resources/fonts/IMPRISHA.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
	}
	
	public Font HeaderTitle_font(){
		return new  Font(default_font, 7, Font.BOLD ,BaseColor.BLACK);
	}
	public Font normal_font(){
		return new  Font(default_font, 7, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font MainTitle_font(){
		return new  Font(default_font, 24, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font Chapter_font(){
		return new  Font(default_font, 16, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font table1B_font(){
		return new  Font(default_font, 10f, Font.BOLD ,BaseColor.BLACK);
	}
	public Font table1_font(){
		return new  Font(default_font, 10f, Font.NORMAL ,BaseColor.BLACK);
	}
	
	//font of front version
	public Font version_font(){
		return new Font(default_font, 10, Font.NORMAL ,BaseColor.GRAY);
	}
	public Font version_font1(){
		return new Font(default_font, 8, Font.NORMAL ,BaseColor.GRAY);
	}
	//font of ligth gray color
	public Font notes_font(){
		return new  Font(default_font, 7, Font.NORMAL ,BaseColor.GRAY);
	}

	public Font notes_font_big(){
		return new  Font(default_font, 8, Font.NORMAL ,BaseColor.GRAY);
	}
	
	//font of title of the fron tpage
	public Font FrontTitle_font(){
		return new  Font(default_font3, 72, Font.BOLD ,BaseColor.BLUE);
	}
	//font of title of the fron tpage
	public Font FrontTitle_font2(){
		return new  Font(default_font, 24, Font.BOLD ,BaseColor.BLUE);
	}
	public Font FrontTitle_fontReport(){
		return new  Font(default_font2, 24, Font.BOLD ,BaseColor.BLUE);
	}
	
	//font of the signature for front page
	public Font Signature_font(){
		return new  Font(default_font2, 16, Font.BOLD ,BaseColor.BLUE);
	}

	//font of title for each chapter
	public Font chapter_title(){
		return new  Font(default_font, 14, Font.BOLD ,BaseColor.BLACK);
	}
	
	//font of subtitle for each chapter
	public Font chapter_subTitle(){
		return new  Font(default_font, 10.5f, Font.BOLD ,BaseColor.BLACK);
	}
	public Font chapterOne_title(){
		return new  Font(default_font, 9, Font.BOLD ,BaseColor.BLACK);
	}
	//font of the content for each chapter
	public Font content_font(){
		return new  Font(default_font, 7.5f, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_font_resIntRight(){
		return new  Font(default_font, 8, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_font_resIntRight1(){
		return new  Font(default_font, 7.5f, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_resIntRight2(){
		return new  Font(default_font, 7.5f, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_font_chapOne(){
		return new  Font(default_font, 7.5f, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_chapThree(){
		return new  Font(default_font, 7.5f, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_font_chapOne1(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_chapOne2(){
		return new  Font(default_font, 7, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font content_font_chapTwo(){
		return new  Font(default_font, 7.5f, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_chapTwo2(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_AppendixHome(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_Catalogue(){
		return new  Font(default_font, 7.5f, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_chapThree(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_chapThree1(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLUE);
	}
	public Font content_font_chapFour(){
		return new  Font(default_font, 8, Font.NORMAL ,BaseColor.BLUE);
	}
	public Font content_font_appendixFive(){
		return new  Font(default_font,7.5f, Font.NORMAL ,BaseColor.BLACK);
	}
	public Font appendixFive_bottom(){
		return new  Font(default_font,7.5f, Font.NORMAL ,BaseColor.GRAY);
	}
	public Font content_font_big(){
		return new  Font(default_font, 12, Font.NORMAL ,BaseColor.BLACK);
		
	}
	public Font content_font_bold(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.BLACK);
	}
	public Font content_font_bold1(){
		return new  Font(default_font, 8, Font.BOLD ,BaseColor.WHITE);
	}

	
	//font of the content for table header
	public Font table_header_font(){
		return new  Font(default_font, 8.5f, Font.NORMAL ,BaseColor.WHITE);
	}
	public Font table_header_title(){
		return new  Font(default_font, 9, Font.BOLD ,BaseColor.WHITE);
	}
	public Font table_header_bold_font(){
		return new  Font(default_font, 9, Font.BOLD ,BaseColor.WHITE);
	}
	
	
	public Font pharseFont(){
		return new  Font(default_font, 9, Font.BOLD ,BaseColor.BLUE);
	}
	public Font pharseFont1(){
		return new  Font(default_font,8.5f, Font.BOLD ,BaseColor.BLUE);
	}
	
}