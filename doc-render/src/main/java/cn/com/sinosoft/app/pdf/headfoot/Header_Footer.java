package cn.com.sinosoft.app.pdf.headfoot;

import java.io.Console;
import java.io.IOException;
import java.net.URL;

import cn.com.sinosoft.app.utils.ResourceLoader;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * Tools: the header and footer
 */
public class Header_Footer {
	
	private String headerImage;
	private String lineImage;
	private String footerImage;
	private String binDir = "";
	public Phrase header;
	public Phrase footer;
	private Fonts_self fonts_self;	

	
	public Header_Footer(String t_binDir){
		binDir = t_binDir;
		//URL headerIma=Header_Footer.class.getResource("/images/bms_logo.jpg");
		//URL footerIma=Header_Footer.class.getResource("/images/footer.gif");
		//headerImage = binDir+headerIma;
		//footerImage = binDir+footerIma;
		//headerImage = "http://192.168.224.217:80/newbms/ibUI/sample/sampleOperate/report/bgi_logo.jpg";
		headerImage = ResourceLoader.getPath("config/images/pic/header_300.png");
		footerImage = ResourceLoader.getPath("config/images/pic/footer_300.png");
		/*headerImage = binDir+"resources/images/header.png";
		footerImage = binDir+"resources/images/footer.gif";*/
		//URL lineIma=Header_Footer.class.getResource("/images/line.png");
		//lineImage = binDir+lineIma;
		/*try {
			fonts_self = new Fonts_self(binDir);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public void go(Document doc, PdfWriter writer, int pageNum){
		//Set the margins
		try{	       
			Image headergif = Image.getInstance(headerImage);
			float Xscale1 = 82f;
	        float Yscale1 = 82f;
	        headergif.scalePercent(Xscale1,Yscale1);
			Chunk headerC1 = new Chunk(headergif, 46, -60);
			
			Image footergif = Image.getInstance(footerImage);
			float Xscale2 = 50f;
	        float Yscale2 = 50f;
	        footergif.scalePercent(Xscale2,Yscale2);
			Chunk footerG1= new Chunk(footergif, 46, -60);
			
			Phrase footer2 = new Phrase();
	        footer2.add(footerG1);
			/*Image linegif = Image.getInstance(lineImage);
			float Xscale2 = 42f;
	        float Yscale2 = 30f;
	        linegif.scalePercent(Xscale2,Yscale2);
			Chunk line = new Chunk(linegif, 200, -10);*/
			
			//Paragraph headerP = new Paragraph("??���??/????��?BGI-TS-04-26-01-001/A0",fonts_self.HeaderTitle_font());
			
			/*Paragraph headerP = new Paragraph("BGI-TS-04-26-01-001/A0",new  Font(FontFamily.TIMES_ROMAN,20, Font.NORMAL ,BaseColor.DARK_GRAY));
			headerP.setSpacingAfter(10);
			headerP.setAlignment(0);*/
			
			
	        header = new Phrase(headerC1);
	        
	 
	        /*Image footergif = Image.getInstance(footerImage);
	        float Xscale2 = 78f;
	        float Yscale2 = 78f;
	        footergif.scalePercent(Xscale2,Yscale2);
	        Chunk footerC1 = new Chunk(footergif,-20,-41);
	        footer = new Phrase(footerC1);
	        footer.add(footerC1);*/
	        Paragraph footerC1 = new Paragraph("", new  Font(FontFamily.TIMES_ROMAN,9, Font.NORMAL ,BaseColor.DARK_GRAY));
	        footerC1.setAlignment(2);
	        //footerC1.setSpacingBefore(160);
	        footer = new Phrase(footerC1);
	        footer.add(footerC1);
	        
	        Rectangle rect = writer.getBoxSize("art");
	        //Rectangle pageSize = PageSize.A4;
	        //The header
			ColumnText.showTextAligned(
					writer.getDirectContent(), 
					Element.ALIGN_LEFT, header, 
					doc.left(), doc.top(), 0);
			//header text
			/*ColumnText.showTextAligned(
					writer.getDirectContent(), 
					Element.ALIGN_RIGHT, 
					new Phrase(headerP), 
					doc.left(), doc.top(), 0);*/
			//System.out.println(doc.left());
			//System.out.println(doc.top());
			/*//line
			ColumnText.showTextAligned(
					writer.getDirectContent(), 
					Element.ALIGN_CENTER, 
					new Phrase(line), 
					rect.getRight()-500, 
					rect.getTop()-40, 0);*/
			//The footer
	        /*ColumnText.showTextAligned(writer.getDirectContent(), 
	        		Element.ALIGN_LEFT, 
	        		footer, 
	        		rect.getLeft(),
	        		rect.getBottom()+41, 
	        		0);*/
	        //The page number
			ColumnText.showTextAligned(
					writer.getDirectContent(), 
					Element.ALIGN_LEFT, footer2, 
					doc.left(), doc.bottom()+70, 0);
			ColumnText.showTextAligned(
					writer.getDirectContent(), 
					Element.ALIGN_CENTER, 
					new Phrase(String.format("%1$-2d\n", pageNum),
				    new  Font(FontFamily.TIMES_ROMAN,9, Font.NORMAL ,BaseColor.DARK_GRAY)),
				    //(doc.rightMargin() + doc.right() + doc.leftMargin() - doc.left()) / 2.0F + 20F,
				    doc.right() -40F,
					doc.bottom()+15,
					0
			);
			/*System.out.println(doc.rightMargin());
			System.out.println(doc.right());
			System.out.println(doc.leftMargin());
			System.out.println(doc.left());
			System.out.println(doc.bottom());*/
		}
		catch (Exception de) {
	        System.out.println("lu"+pageNum);
	        System.err.println(de.getMessage());
		}
	}
}
