package cn.com.sinosoft.app.pdf.headfoot;


import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Tools: page
 */
public class PageCounter implements PdfPageEvent {
	PdfTemplate chapter_indexs[] = new PdfTemplate[10];
	private String binDir = "";
	public void set_binDir(String t_binDir){
		binDir = t_binDir;
	}
	public void create_chapter_template(int chapter_num, PdfWriter writer,float x,float y){
		if(chapter_num >= 10){
			System.out.println("chapter_num >= 10");
			return;
		}
		chapter_indexs[chapter_num] = writer.getDirectContent().createTemplate(100, 100);
		writer.getDirectContent().addTemplate(chapter_indexs[chapter_num], x, y);
	}
	

	@SuppressWarnings("unused")
	public void write_chapter_template(int chapter_num, PdfWriter writer,Font t_font){
		chapter_indexs[chapter_num].beginText();
		Fonts_self o_fonts ;
		try {
			o_fonts = new Fonts_self("");
			chapter_indexs[chapter_num].setFontAndSize(t_font.getBaseFont(), 10);
	        String t_pageNum = ""+(writer.getPageNumber() - 1);
	        chapter_indexs[chapter_num].showText(t_pageNum);
	        chapter_indexs[chapter_num].endText();
	        chapter_indexs[chapter_num].closePath();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	public void write_chapter_template_report(int chapter_num, PdfWriter writer,Font t_font){
		chapter_indexs[chapter_num].beginText();
		Fonts_self o_fonts ;
		try {
			o_fonts = new Fonts_self("");
			chapter_indexs[chapter_num].setFontAndSize(t_font.getBaseFont(), 10);
	        //String t_pageNum = ""+(writer.getPageNumber() - 1);
			String t_pageNum = ""+writer.getPageNumber();
	        chapter_indexs[chapter_num].setColorFill(BaseColor.WHITE);
	        chapter_indexs[chapter_num].showText(t_pageNum);
	        chapter_indexs[chapter_num].endText();
	        chapter_indexs[chapter_num].closePath();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public void onGenericTag(PdfWriter paramPdfWriter, Document paramDocument, Rectangle paramRectangle, String paramString) {
		// TODO Auto-generated method stub
		
	}
	public void onOpenDocument(PdfWriter paramPdfWriter, Document paramDocument) {
		// TODO Auto-generated method stub
		
	}
	public void onParagraph(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat) {
		// TODO Auto-generated method stub
		
	}
	public void onParagraphEnd(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat) {
		// TODO Auto-generated method stub
		
	}
	public void onSection(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat, int paramInt, Paragraph paramParagraph) {
		// TODO Auto-generated method stub
		
	}
	public void onSectionEnd(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat) {
		// TODO Auto-generated method stub
		
	}
	public void onStartPage(PdfWriter paramPdfWriter, Document paramDocument) {
		// TODO Auto-generated method stub
		System.out.println("start page");
	}
	public void onEndPage(PdfWriter paramPdfWriter, Document paramDocument) {
		//no header & footer for front page
		if( paramPdfWriter.getCurrentPageNumber() == 1 )
					return;
		//initialize header & footer
		Header_Footer o_hf = new Header_Footer(binDir);
		//o_hf.go(doc, writer, writer.getCurrentPageNumber()-1);
		o_hf.go(paramDocument, paramPdfWriter, paramPdfWriter.getCurrentPageNumber()-1);
		System.out.println("current page:" + paramPdfWriter.getCurrentPageNumber());

	}
	public void onCloseDocument(PdfWriter paramPdfWriter, Document paramDocument) {
		// TODO Auto-generated method stub
		
	}
	public void onChapter(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat, Paragraph paramParagraph) {
		// TODO Auto-generated method stub
		
	}
	public void onChapterEnd(PdfWriter paramPdfWriter, Document paramDocument, float paramFloat) {
		// TODO Auto-generated method stub
		
	}

	

}
